class CreatePromotions < ActiveRecord::Migration[5.1]
  def change
    create_table :promotions do |t|
      t.string :code
      t.integer :offer

      t.timestamps
    end
  end
end
