class Admin::CategoryController < ApplicationController
  before_action :authenticate_user!
  def add
    if params[:name]
      Category.create! :name => params[:name]
      render :json => {
        success: true,
        message: "Category Created"
      }
    else
      render :json => {
        success: false,
        message: "Invalid params"
      }
    end
  end

  def edit
    if params[:id] && params[:name]
      Product.where(:id => params[:id]).first.update_attributes! :name => params[:name]
      render :json => {
        success: true,
        message: "Category Updated"
      }
    else
      render :json => {
        success: false,
        message: "Invalid params"
      }
    end
  end

  def delete
    if params[:id]
      Category.where(:id => params[:id]).first.destroy
      render :json => {
        success: true,
        message: "Category Deleted"
      }
    else
      render :json => {
        success: false,
        message: "Invalid params"
      }
    end
  end
end
