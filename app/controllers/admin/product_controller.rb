class Admin::ProductController < ApplicationController
  before_action :authenticate_user!
  def add
    if params[:name] && params[:price] && params[:category]
      Product.create! :name => params[:name], :price => params[:price], :category_id => params[:category]
      render :json => {
        success: true,
        message: "Product Created"
      }
    else
      render :json => {
        success: false,
        message: "Invalid params"
      }
    end
  end

  def edit
    if params[:id] && params[:name] && params[:price]
      Product.where(:id => params[:id]).first.update_attributes! :name => params[:name], :price => params[:price]
      render :json => {
        success: true,
        message: "Product Updated"
      }
    else
      render :json => {
        success: false,
        message: "Invalid params"
      }
    end
  end

  def delete
    if params[:id]
      Product.where(:id => params[:id]).first.destroy
      render :json => {
        success: true,
        message: "Product Deleted"
      }
    else
      render :json => {
        success: false,
        message: "Invalid params"
      }
    end
  end
end
