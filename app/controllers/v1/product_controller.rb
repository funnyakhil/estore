class V1::ProductController < ApplicationController
  before_action :authenticate_user!
  def all
    if params[:category]
      render :json => Product.select(:id, :name, :price).where(:category_id => params[:category]).limit(10)
    else
      render :json => Product.select(:id, :name, :price).limit(10)
    end
  end

  def one
    p = Product.select(:id, :name, :price).where(:id => params[:id]).first
    r = User.joins(:reviews).select("reviews.id, reviews.title, reviews.desc, users.name").where("reviews.product_id = ?", params[:id])
    render :json => p.as_json.merge({reviews: r})
  end

  def search
    if params[:query]
      render :json => Product.select(:id, :name, :price).where('LOWER(name) LIKE ?', "%#{params[:query].downcase}%")
    else
      render :json => []
    end
  end
end
