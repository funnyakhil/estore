class V1::AddressController < ApplicationController
  before_action :authenticate_user!
  def all
    render :json => Address.select(:id, :line1, :line2, :city, :state, :zip, :country).where(:user_id => current_user.id)
  end

  def add
    if params[:line1] && params[:line2] && params[:city] && params[:state] && params[:zip] && params[:country]
      Address.create! :line1=>params[:line1],:line2=>params[:line2],:city=>params[:city],:state=>params[:state],:zip=>params[:zip],:country=>params[:country],:user_id=>current_user.id
      render :json => {
        success: true,
        message: "Address Added"
      }
    else
      render :json => {
        success: false,
        message: "Invalid params"
      }
    end
  end

  def edit
    if params[:id] && params[:line1] && params[:line2] && params[:city] && params[:state] && params[:zip] && params[:country]
      a = Address.where(:id => params[:id]).first
      if a.user_id == current_user.id
        a.update_attributes! :line1=>params[:line1],:line2=>params[:line2],:city=>params[:city],:state=>params[:state],:zip=>params[:zip],:country=>params[:country]
        render :json => {
          success: true,
          message: "Address Updated"
        }
      else
        render :json => {
          success: false,
          message: "Unauthorized"
        }
      end
    else
      render :json => {
        success: false,
        message: "Invalid params"
      }
    end
  end

  def delete
    if params[:id]
      a = Address.where(:id => params[:id]).first
      if a.user_id == current_user.id
        a.destroy!
        render :json => {
          success: true,
          message: "Address Deleted"
        }
      else
        render :json => {
          success: false,
          message: "Unauthorized"
        }
      end
    else
      render :json => {
        success: false,
        message: "Invalid params"
      }
    end
  end
end
