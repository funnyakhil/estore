class V1::CategoryController < ApplicationController
  before_action :authenticate_user!
  def all
    render :json => Category.select(:id, :name)
  end
end
