class V1::CartController < ApplicationController
  before_action :authenticate_user!
  def cart
    c = Cart.where(:user_id => current_user.id, :removed => false).pluck(:id)
    render :json => Product.select(:id, :name, :price).where(:id => c)
  end

  def add
    if params[:product]
      Cart.create! :product_id=>params[:product],:user_id=>current_user.id
      render :json => {
        success: true,
        message: "Product added to cart"
      }
    else
      render :json => {
        success: false,
        message: "Invalid params"
      }
    end
  end

  def remove
    if params[:product]
      Cart.where(:product_id => params[:product], :user_id => current_user.id, :removed => false).first.destroy
      render :json => {
        success: true,
        message: "Product removed from cart"
      }
    else
      render :json => {
        success: false,
        message: "Invalid params"
      }
    end
  end
end
