class V1::OrderController < ApplicationController
  before_action :authenticate_user!
  def promotion
    if params[:code]
      render :json => Promotion.select(:id, :code, :offer).where('LOWER(code) = ?', params[:code].downcase).first
    end
  end

  def orders
    render :json => Order.select(:id, :pay_ref, :paid).where(:user_id => current_user.id)
  end

  def order
    if params[:order]
      o = Order.select(:id, :pay_ref, :paid, :address_id).where(:id => order).first
      c = Cart.where(:order_id => params[:order]).pluck(:product_id)
      render :json => {
        details: o,
        items: Product.select(:id, :name, :price).where(:id => c),
        address: o.address
      }
    else
      render :json => {
        success: false,
        message: "Invalid params"
      }
    end
  end

  def pay
    if params[:pay_ref] && params[:paid] && params[:address] && params[:promotion]
      o=Order.new(:paid => params[:paid], :pay_ref => params[:pay_ref], :address_id => params[:address], :promotion_id => params[:promotion], :user_id => current_user.id)
      o.save!
      Cart.where(:user_id => current_user.id, :removed => false).update_all :order_id => o.id
      render :json => {
        success: true,
        message: "Order placed"
      }
    else
      render :json => {
        success: false,
        message: "Invalid params"
      }
    end
  end
end
