Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'
  namespace :v1 do
    # Get products and categories
    get 'category/all'
    get 'product/all'
    get 'product/:id', to: 'product#one'
    # Search by product name
    get 'search/:query', to: 'product#search'
    # Address CRUD
    get '/address/all'
    post 'address/add'
    put 'address/edit/:id', to: 'address#edit'
    delete 'address/delete/:id', to: 'address#delete'
    # Cart CRUD
    get 'cart', to: 'cart#cart'
    post 'cart/add'
    delete 'cart/remove/:id', to: 'cart#remove'
    # Apply promotion
    post 'promo/new', to: 'order#promotion'
    # Return from payment gateway
    post 'payConfirm', to: 'order#pay'
    # My orders
    get 'orders', to: 'order#orders'
    # Order details
    get 'order/:order', to: 'order#order'
  end
  namespace :admin do
    # Product CRUD
    post 'product/add'
    put 'product/edit/:id', to: 'product#edit'
    delete 'product/delete/:id', to: 'product#delete'
    # Category CRUD
    post 'category/add'
    put 'category/edit/:id', to: 'category#edit'
    delete 'category/delete/:id', to: 'category#delete'
  end
end
